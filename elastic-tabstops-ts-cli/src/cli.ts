#!/usr/bin/env node

import { Command } from 'commander'
import fs from 'fs'
import path from 'path'

import { calcElasticTabstopsMonospaced, getCellsPerLine, getTextPerLine, Options } from 'elastic-tabstops'

const defaultConfig: Options =
{
	spacing	: { pad: 2, min: 0, mod: 0 },
	tabSize	: 4,
	indentSpaces	: 4,
	ignoreSpaces	: true,
	indentAware	: true,
	alignNumbers	: true,
}

const usage = '\
Format textfiles with elastic tabstops\n\
'

const examples = '\n\
Examples:\n\
ets -i input.txt -c conf.json -t 4 -s 2 -o output.txt\n\
'

const program = new Command()
program
	.version('0.1')
	.description(usage)
	//.option('-v	, --verbose'	, 'Verbose mode')
	.option('-i, --input <path>'	, 'Input file (optional, default from stdin)')
	.option('-c, --config [path]'	, 'Config JSON file (optional), empty path prints default config', false)
	.option('-o, --output <path>'	, 'Output file (optional, default to stdout)')
	.option('-t, --tabsize <integer>'	, 'Tabsize of indent tabs'	, defaultConfig.tabSize	.toString())
	.option('-s, --spaces <integer>'	, 'Amount of spaces used for indent'	, defaultConfig.indentSpaces	.toString())
	.option('--pad <integer>'	, 'Padding which gets appended to every cell'	, defaultConfig.spacing.pad	.toString())
	.option('--min <integer>'	, 'Minimum width of cell (after adding padding)'	, defaultConfig.spacing.min	.toString())
	.option('--mod <integer>'	, 'Expand cell to next modulos column (0=disabled)'	, defaultConfig.spacing.mod	.toString())
	.option('--no-ignoreSpaces'	, "Don't ignore alignment spaces before and after elastic tabs")
	.option('--no-indentAware'	, 'Disable indent-aware elastic tabstops')
	.option('--no-alignNumbers'	, "Don't right-align numbers")
	.option('-w, --whitespace'	, "Print special whitespace characters")
	.option('-r, --remove'	, 'Just remove alignment spaces (ignores most other options)')
	.addHelpText('after', examples)
	.showHelpAfterError(false)
	.exitOverride((err) => {
		if (err.code === 'commander.unknownOption')
		{
			program.outputHelp()
			console.error("\n" + err.message)
		}
		process.exit(err.exitCode)
	})
	.parse(process.argv)

const opts = program.opts()

main()
.catch(err => {
	program.outputHelp()
	console.error('Error: ', err)
	process.exit(1)
})

async function readInput(inputPath: string)
{
	if (inputPath)
	{
		return fs.promises.readFile(inputPath, 'utf-8')
	}
	else
	{
		process.stdin.setEncoding('utf8')
		let _ret = ''
		if (process.stdin.isTTY)
		{
			console.log("Enter text (press Ctrl+D to finish or Ctrl+C to cancel):")
		}
		for await (const chunk of process.stdin)
		{
			_ret += chunk
		}
		return _ret
	}
}

function writeOutput(outputPath: string, outputText: string)
{
	if (outputPath)
	{
		fs.writeFileSync(outputPath, outputText)
	}
	else
	{
		console.log(outputText)
	}
}

async function main()
{
	let _conf: Options = { ...defaultConfig }
	if (typeof(opts.config) === 'string')
	{
		try
		{
			const configFileContent	= fs.readFileSync(path.resolve(opts.config), 'utf-8')
			const configFileJson	= JSON.parse(configFileContent)
			_conf = { ..._conf, ...configFileJson }
		}
		catch (e)
		{
			console.warn(`Cannot read config file: '${opts.config}'. Proceeding with default config.`, e)
		}
	}
	else if (opts.config)
	{
		console.log(`Default config:\n${JSON.stringify(defaultConfig, null, 4)}`)
	}

	if (opts.pad	!= null) { _conf.spacing.pad	= parseInt(opts.pad	) }
	if (opts.min	!= null) { _conf.spacing.min	= parseInt(opts.min	) }
	if (opts.mod	!= null) { _conf.spacing.mod	= parseInt(opts.mod	) }
	if (opts.tabsize	!= null) { _conf.tabSize	= parseInt(opts.tabsize	) }
	if (opts.spaces	!= null) { _conf.indentSpaces	= parseInt(opts.spaces	) }

	if (opts.ignoreSpaces	) { _conf.ignoreSpaces	= opts.ignoreSpaces	}
	if (opts.indentAware	) { _conf.indentAware	= opts.indentAware	}
	if (opts.alignNumbers	) { _conf.alignNumbers	= opts.alignNumbers	}

	const inputText	= await readInput(opts.input)
	const textPerLine	= getTextPerLine(inputText)
	const cellsPerLine	= getCellsPerLine(textPerLine, _conf.indentSpaces)
	const elasticTabstops	= calcElasticTabstopsMonospaced(textPerLine, _conf)

	const outputText	= cellsPerLine.map((line, l) => line.map((cell_, c) => {
		const ets = elasticTabstops[l][c]
		const cell = opts.ignoreSpaces || opts.remove ? cell_.trim() : cell_
		if (ets == null	) { return cell }
		if (opts.remove	) { return cell + '\t' }
		if (opts.whitespace	)
		{
			if (opts.isIndent	) { return cell + "→" + " ".repeat(ets.widthEts - 1) }
			if (opts.widthEts === 0	) { return cell }
			return cell + "↔" + " ".repeat(ets.widthEts)
		}
		if (opts.isIndent) { return cell + '\t' }
		return cell + " ".repeat(ets.widthEts)
	}).join('')).join('\n')

	writeOutput(opts.output, outputText)
}
