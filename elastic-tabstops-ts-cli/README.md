# Elastic Tabstops CLI

Commandline tool to format textfiles with elastic tabstops

```
cd elastic-tabstops-ts-cli
npm install
npm run build
```

you can run it with either `node out/index.js ...` or `npx ts-node src/index.ts ...` (without building).

## Usage

```
Usage: index [options]

Format textfiles with elastic tabstops

Options:
  -V, --version            output the version number
  -i, --input <path>       Input file (optional, default from stdin)
  -c, --config [path]      Config JSON file (optional), empty path prints default config (default: false)
  -o, --output <path>      Output file (optional, default to stdout)
  -t, --tabsize <integer>  Tabsize of indent tabs (default: "4")
  -s, --spaces <integer>   Amount of spaces used for indent (default: "4")
  --pad <integer>          Padding which gets appended to every cell (default: "2")
  --min <integer>          Minimum width of cell (after adding padding) (default: "0")
  --mod <integer>          Expand cell to next modulos column (0=disabled) (default: "0")
  --no-ignoreSpaces        Don't ignore alignment spaces before and after elastic tabs
  --no-indentAware         Disable indent-aware elastic tabstops
  --no-alignNumbers        Don't right-align numbers
  -w, --whitespace         Print special whitespace characters
  -h, --help               display help for command

Examples:
ets -i input.txt -c conf.json -t 4 -s 2 -o output.txt
```

## Default config

```
{
	"spacing"     : { "pad": 2, "min": 0, "mod": 0 },
	"tabSize"     : 4,
	"indentSpaces": 4,
	"ignoreSpaces": true,
	"indentAware" : true,
	"alignNumbers": true
}
```
