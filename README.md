# Elastic Tabstops

*A better way to indent and align code*

Based on the [idea of Nick Gravgaard](https://nickgravgaard.com/elastic-tabstops).

![animation](columnblocks_coloured.gif)

*This animated diagram shows how the elastic tabstops mechanism aligns text. A tab character is represented as a vertical line. Each cell ends with a tab character. A column block is a run of uninterrupted vertically adjacent cells. A column block is as wide as the widest piece of text in the cells it contains or a minimum width (plus padding). Text outside column blocks is ignored.* -- Nick Gravgaard

This is a monorepo consisting of:

* `docs/`: [Elastic Tabstops Documentation](https://elastic-tabstops.readthedocs.io) describing the idea, algorithm, implementations etc.
* `elastic-tabstops-ts/`: Typescript library for elastic tabstops
* `elastic-tabstops-ts-cli/`: Commandline tool to format a textfile with elastic tabstops (using the typescript library)
* `vsce/`: Visual Studio Code extension (on [Vscode marketplace](https://marketplace.visualstudio.com/items?itemName=gerold-meisinger.elastic-tabstops-lite-redux))

For related projects see: <https://elastic-tabstops.readthedocs.io/editors.html>
