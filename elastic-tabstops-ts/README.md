# Elastic tabstops library for Typescript

Based on the [idea of Nick Gravgaard](https://nickgravgaard.com/elastic-tabstops).

![animation](elastictabstops.gif)

**[Read Elastic Tabstops Documentation](https://elastic-tabstops.readthedocs.io)**

```
const elasticTabstops = calcElasticTabstopsMonospaced(text)
```

For more example usage see `/elastic-tabstops-ts-cli` and `/vsce`.
