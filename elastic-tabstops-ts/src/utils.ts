import { type CharIdx, type LineIdx, type Position, type Range, type Tabstop, type VisPosEts } from './types'

export function stringifyElastic<T> (arr2: T[][]): string
{
	const toString = (v: T): string =>
	{
		if (v	==  null	) { return '_'	}
		if (typeof v	=== 'number'	) { return v.toString()	}
		return JSON.stringify(v)
	}

	if (arr2.length === 0) { return '[]' }

	const lengthMax	= Math.max(...arr2.map(arr1 => arr1.length))
	const columnWidthsMax	= Array.from({ length: lengthMax }, (_, idx) => Math.max(...arr2.map(arr1 => idx < arr1.length ? toString(arr1[idx]).length : 0)))

	// console.log(columnWidthsMax.toString())
	const lines = arr2.map(arr1 =>
	{
		const line = arr1.map((v, c) =>
		{
			const str = toString(v)
			if (v == null || typeof v === 'number')	{ return str.padStart	(columnWidthsMax[c], ' ') }
			else	{ return str.padEnd	(columnWidthsMax[c], ' ') }
		})
		const lineStr = `[ ${line.join(', ')}`
		return lineStr
	})
	const lineWidthMax	= Math.max(...lines.map(l => l.length))
	const linesStr	= `[ ${lines.map(l => `${l.padEnd(lineWidthMax + 1, ' ')}]`).join('\n, ')}\n]`
	return linesStr
}

export function indexOfAll (text: string, searchElement: string): number[]
{
	const ret = []
	for (let i = 0; i < text.length; i++)
	{
		if (text[i] === searchElement)
		{
			ret.push(i)
		}
	}
	return ret
}

/**
 * Get start and end positions of all spaces length >= 2 for each text line.
 * This can be used to convert a `spaces-n` or `spaces-aligned` text back to elastic tabstops.
 * Also see the accompaning chapter 'Converting aligned spaces to elastic tabstops' in implementation.md.
 *
 * ```
 * →→aa············ddddd······gggggggg··jjj    [ [ 4-16, 21-27, 35-37] ]
 * →→bbbbbbb·······e··········hhhhhh····kkk => , [ 9-16, 17-27, 33-37]
 * →→cccccccccccc··fffffffff··iiii······lll    , [14-16, 25-27, 31-37]
 *                                               ]
 * ```
 * @param textPerLine
 * @returns
 */
export function getSpacePositions (textPerLine: string[]): [CharIdx[][], CharIdx[][]]
{
	// TODO: check if this works with spaces used for indentation
	const regex = / {2,}/g

	const startsPerLine	= []
	const endsPerLine	= []
	for (let l = 0; l < textPerLine.length; l++)
	{
		const startsOfLine	= []
		const endsOfLine	= []

		const textOfLine = textPerLine[l]
		let _match
		while ((_match = regex.exec(textOfLine)) != null)
		{
			startsOfLine	.push(_match.index)
			endsOfLine	.push(_match.index + _match[0].length)
		}

		startsPerLine	.push(startsOfLine)
		endsPerLine	.push(endsOfLine)
	}

	return [startsPerLine, endsPerLine]
}

/**
 * Get all elastic tabstop positions in the endPositions of an `spaces-aligned` text
 * Also see the accompaning chapter 'Converting aligned spaces to elastic tabstops' in implementation.md.
 * @param endPositions
 * @returns
 */
export function getTabPositions (endPositions: CharIdx[][]): CharIdx[][]
{
	const uniqPositions	= [...new Set(endPositions.flat())] // use Set so we only get unique positions of ALL endPositions

	const tabPositions: Array<Set<CharIdx>> = Array.from({ length: endPositions.length }, () => new Set())
	for (let c = 0; c < uniqPositions.length; c++)
	{
		for (let l = 0; l < endPositions.length; l++)
		{
			const endLine = endPositions[l]
			if (c >= endLine.length) { continue } // skip if column is already beyond line length

			const run = new Set<CharIdx>()
			let _endRun = l
			for (; c < endPositions[_endRun].length; _endRun++)
			{
				const end = endPositions[_endRun][c]
				run.add(end)
			}
			const ends = [...run].sort((a , b) => a - b)

			for (const e of ends)
			{
				for (let j = l; j < _endRun; j++)
				{
					if (e > endPositions[j][c]) { continue } // skip if tabstop is already beyond spaces
					const tabLine = tabPositions[j]
					tabLine.add(e)
				}
			}
			l = _endRun
		}
	}
	return tabPositions.map(l => [...l])
}

/**
 * Convert a spaces-aligned text to tabs which can be used as elastic tabstops.
 * See {@link convertToSpaces} for inverse.
 * Also see the accompaning chapter 'Converting aligned spaces to elastic tabstops' in implementation.md.
 *
 * ```
 * →   →   aa············ddddd······gggggggg··jjj
 * →   →   bbbbbbb·······e··········hhhhhh····kkk
 * →   →   cccccccccccc··fffffffff··iiii······lll
 * ```
 *
 * becomes:
 *
 * ```
 * →   →   aa↔           ddddd↔     gggggggg↔ jjj
 * →   →   bbbbbbb↔      e↔         hhhhhh↔   kkk
 * →   →   cccccccccccc↔ fffffffff↔ iiii↔     lll
 * ```
 */
export function convertToTabs (textPerLine: string[]): string[]
{
	const [startPositions, endPositions]	= getSpacePositions(textPerLine)
	const tabPositions	= getTabPositions(endPositions)

	const tabsPerLine = textPerLine.map((textOfLine, l) =>
	{
		const startLine	= startPositions	[l]
		const endLine	= endPositions	[l]
		const tabLine	= tabPositions	[l]

		if (startLine.length === 0) { return textOfLine } // no spaces-n found in text

		let _newLine = textOfLine.substring(0, startLine[0]) // copy text until first start
		for (let i = 0; i < startLine.length; i++)
		{
			const start	= startLine	[i]
			const end	= endLine	[i]

			/* Insert tabs for all tab positions which are between start and end position of spaces.
			 * Keep in mind that the spaces can cross multiple elastic tabstops used for empty cells.
			 * ```
			 * aaa··ddd··ggg    aaa↔ ddd↔ ggg
			 * ·····eee··hhh => ↔    eee↔ hhh
			 * ··········iii    ↔    ↔  ↔ iii
			 * ```
			 */
			for (let t = 0; t < tabLine.length; t++)
			{
				const tabPos = tabLine[t]
				if (tabPos >= start && tabPos <= end) {
					_newLine += '\t'
				}
			}

			if (i < startLine.length - 1) // copy text until next start
			{
				_newLine += textOfLine.substring(end, startLine[i + 1])
			}
			else // copy text until end of line
			{
				_newLine += textOfLine.substring(end, textOfLine.length)
			}
		}
		return _newLine
	})
	return tabsPerLine
}

/**
 * Convert a text using elastic tabstops to a spaces-aligned text.
 * Make sure to use spaces-n with at least 2 spaces to be able to convert it back to tabs again.
 *
 * See {@link convertToTabs} for inverse.
 *
 * ```
 * →   →   aa↔           ddddd↔     gggggggg↔ jjj
 * →   →   bbbbbbb↔      e↔         hhhhhh↔   kkk
 * →   →   cccccccccccc↔ fffffffff↔ iiii↔     lll
 * ```
 *
 * becomes:
 *
 * ```
 * →   →   aa············ddddd······gggggggg··jjj
 * →   →   bbbbbbb·······e··········hhhhhh····kkk
 * →   →   cccccccccccc··fffffffff··iiii······lll
 * ```
 *
 * @param textPerLine - Text lines as an array of strings
 * @param elasticTabstops - A 2D array of elastic tabstops corresponding to each line
 * @returns An array of strings with tabs converted to spaces
 */
function convertToSpaces (textPerLine: string[], elasticTabstops: Tabstop[][]): string[]
{
	const spacesPerLine: string[] = []
	for (let l = 0; l < textPerLine.length; l++)
	{
		const textOfLine = textPerLine[l]

		let _start	= 0
		let _newLine	= ''
		for (let t = 0; t < elasticTabstops[l].length; t++)
		{
			const tabstop = elasticTabstops[l][t]
			_newLine += `${textOfLine.substring(_start, tabstop.charIdx)}${' '.repeat(tabstop.widthEts)}`
			_start = tabstop.charIdx + 1
		}
		_newLine += textOfLine.substring(_start, textOfLine.length)
		spacesPerLine.push(_newLine)
	}
	return spacesPerLine
}

// export function visPosMod2visPosEts (elasticTabstopsLine: ElasticTabstop[], visPodMod: number): number {
//	const ets = elasticTabstopsLine.findLast(ets => ets.visPosMod < visPodMod)
//	if (ets == null) { return visPodMod }
//	const offset	= visPodMod - (ets.visPosMod + ets.widthUnelastic)
//	const visPosEts	= ets.visPosMod + ets.width + offset
//	return visPosEts
// }

// export function visPosEts2visPosMod (elasticTabstopsLine: ElasticTabstop[], visPosEts: number): number {
//	const ets = elasticTabstopsLine.findLast(ets => ets.visPosMod < visPosEts)
//	if (ets == null) { return visPosEts }
//	const offset	= visPosEts - ets.visPosMod
//	const visPosMod	= ets.visPosMod + ets.widthUnelastic + offset
//	return visPosMod
// }

export function charIdx2visPosEts (tabstopsLine: Tabstop[], charIdx: CharIdx): VisPosEts
{
	const ts = tabstopsLine.findLast(ts => ts.charIdx <= Math.round(charIdx))
	if (ts == null) { return charIdx }

	if (charIdx === ts.charIdx) { return ts.visPosEts }

	const offset	= charIdx - ts.charIdx
	const visPosEts	= ts.visPosEts + ts.widthEts + offset - 1
	return visPosEts
}

export function visPosEts2charIdx (tabstopsLine: Tabstop[], visPosEts: VisPosEts, mode: 'floor' | 'ceil' | 'round' = 'round'): CharIdx
{
	//   0123456789A123456789B123456789C123456789D
	// 0:→   12↔           45678↔     A1234567↔ 9B1
	// 1:→   1234567↔      9↔         123456↔   89B
	// 2:→   123456789A12↔ 456789B12↔ 4567↔     9B1
	const ts = tabstopsLine.findLast(ts => ts.visPosEts <= visPosEts)
	console.log(JSON.stringify(ts))
	if (ts == null) { return visPosEts }

	const offset	= visPosEts - (ts.visPosEts + ts.widthEts)
	console.log(offset)
	if (offset >= 0) { return ts.charIdx + offset + 1 }

	switch (mode)
	{
		case 'floor'	: return ts.charIdx
		case 'ceil'	: return ts.charIdx + 1
		case 'round'	:
			if (ts.widthEts + offset < ts.widthEts * 0.5) { return ts.charIdx }
			return ts.charIdx + 1
	}
}

export function getTabstopByVisPosEts (tabstopsLine: Tabstop[], visPosEts: VisPosEts): Tabstop | undefined
{
	const ret = tabstopsLine.findLast(ts => ts.visPosEts <= visPosEts)
	if (ret == null) { return undefined }
	return ret
}

export function getRangesOfTabsWithSpaces (textPerLine: string[]): Range[][]
{
	const regex = / *(?<!^(?:\t)*)\t */g
	const _ret: Range[][] = textPerLine.map((line, l) => {
		const	_rangesOfLine = []
		let	_match: RegExpExecArray | null
		while ((_match = regex.exec(line)) != null)
		{
			const start	= _match.index
			const end	= _match[0].length + start
			const range: Range = [[l, start], [l, end]]
			_rangesOfLine.push(range)
		}
		return _rangesOfLine
	})

	return _ret
}

export function replaceElasticTabstopsWithSpaces (elasticTabstops: Tabstop[][]): Array<[Range, string]>
{
	const ret: Array<[Range, string]> = []
	for (let l = 0; l < elasticTabstops.length; l++)
	{
		for (let t = 0; t < elasticTabstops[l].length; t++)
		{
			const { isIndent, charIdx, widthEts }	= elasticTabstops[l][t]
			if (isIndent) { continue }

			const startPos	: Position = [l, charIdx + 0]
			const endPos	: Position = [l, charIdx + 1]
			const str	= ' '.repeat(widthEts)
			ret.push([[startPos, endPos], str])
		}
	}
	return ret
}

export function replaceSpacesFromElasticTabstops (elasticTabstops: Tabstop[][]): Array<[Range, string]>
{
	const ret: Array<[Range, string]> = []
	for (let l = 0; l < elasticTabstops.length; l++)
	{
		for (let t = 0; t < elasticTabstops[l].length; t++)
		{
			const { visPosEts, widthEts }	= elasticTabstops[l][t]

			const startPos	: Position = [l, visPosEts + 0	]
			const endPos	: Position = [l, visPosEts + widthEts	]
			ret.push([[startPos, endPos], '\t'])
		}
	}
	return ret
}

export function replaceSpacesWithElasticTabstops (textPerLine: string[]): Array<[Range, string]>
{
	const [startPositions, endPositions]	= getSpacePositions(textPerLine)
	const tabPositions	= getTabPositions(endPositions)

	const replacements: Array<[Range, string]> = []
	for (let l = 0; l < textPerLine.length; l++)
	{
		const startLine	= startPositions	[l]
		const endLine	= endPositions	[l]
		const tabLine	= tabPositions	[l]
		if (tabLine.length === 0) { continue }

		for (let i = 0; i < startLine.length; i++)
		{
			const start	= startLine	[i]
			const end	= endLine	[i]

			let tabs = 0
			for (let t = 0; t < tabLine.length; t++)
			{
				if (tabLine[t] >= start && tabLine[t] <= end) { tabs++ }
			}
			const startPos	: Position	= [l, start	]
			const endPos	: Position	= [l, end	]
			const str	= '\t'.repeat(tabs)
			replacements.push([[startPos, endPos], str])
		}
	}
	return replacements
}

export function getIndentLevels (textPerLine: string[], spaces = 0): number[]
{
	const ret = textPerLine.map(line => {
		let _tabCount	= 0
		let _spaceCount	= 0

		for (const char of line)
		{
			if	(char === '\t'	) { _tabCount++	}
			else if	(char === ' '	) { _spaceCount++	}
			else { break }
		}

		const indent = spaces === 0 ? _tabCount : _tabCount + Math.floor(_spaceCount / spaces)
		return indent
	})
	return ret
}

export function expandToSameIndent (textPerLine: string[], line: LineIdx, spaces = 0): [LineIdx, LineIdx]
{
	const indentLevels = getIndentLevels(textPerLine, spaces)
	const level = indentLevels[line]

	let _startLine	= line
	let _endLine	= line

	for (let i = line; i >= 0; i--)
	{
		if (indentLevels[i] !== level) { break }
		_startLine = i
	}

	for (let i = line + 1; i < textPerLine.length; i++)
	{
		if (indentLevels[i] !== level) { break }
		_endLine = i
	}

	const ret : [LineIdx, LineIdx] = [_startLine, _endLine]
	return ret
}

export function findDelimiterPositions (textPerLine: string[], delimiters: string[], spaces = 0, start = true): CharIdx[][]
{
	const ret = textPerLine.map(line => {
		const positions: CharIdx[] = []
		let _offset = 0

		const spacesStr = ' '.repeat(spaces)
		while (_offset < line.length)
		{
			if	(line.startsWith('\t'	, _offset))	{ _offset += 1	}
			else if	(line.startsWith(spacesStr	, _offset) && spaces > 0)	{ _offset += spaces	}
			else break
		}

		let _hasChar = false
		while (_offset < line.length)
		{
			let _found = false
			for (const delimiter of delimiters)
			{
				if (line.startsWith(delimiter, _offset) && ((start && line[_offset - 1] !== '\t') || (!start && line[_offset + delimiter.length] !== '\t')))
				{
					if (_hasChar)
					{
						positions.push(start ? _offset : _offset + delimiter.length)
						_offset += delimiter.length
						_found = true
						break
					}
				}

			}

			if (!_found)
			{
				_offset++
				_hasChar = true
			}
		}

		return positions.reverse()
	})

	return ret
}
