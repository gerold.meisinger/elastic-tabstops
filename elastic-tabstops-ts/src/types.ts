export type LineIdx	= number
export type CharIdx	= number
export type VisPosEts	= number
export type VisPosMod	= number

export type Position	= [LineIdx, CharIdx]
export type Range	= [Position, Position]

export type TextPerLine	= string[]
export type CellsPerLine	= TextPerLine[]

export interface Tabstop
{
	isIndent	: boolean	// whether the tab is an indendation tab or elastic tabstop
	level	: number	// indentation level
	charIdx	: CharIdx	// index position of `\t` char in the string ("character array")
	visPosEts	: VisPosEts	// visual column position (=if text would be all spaces) of character with respect to tabsize
	visPosMod	: VisPosMod	// visual column position according to classic tab calculation (modulo)
	widthEts	: number	// desired spacing for elastic tabstop
	widthMod	: number	// spacing for classic tab calcualation (modulo)
}

export interface Spacing
{
	pad	: number
	min	: number
	mod	: number
}

export const mod2	: Spacing = { pad: 1, min: 0, mod: 2 }
export const mod4	: Spacing = { pad: 1, min: 0, mod: 4 }
export const mod8	: Spacing = { pad: 1, min: 0, mod: 8 }
export const spaces0	: Spacing = { pad: 0, min: 0, mod: 0 }
export const spaces1	: Spacing = { pad: 1, min: 0, mod: 0 }
export const spaces2	: Spacing = { pad: 2, min: 0, mod: 0 }
export const spaces4	: Spacing = { pad: 4, min: 0, mod: 0 }
export const reference	: Spacing = { pad: 2, min: 4, mod: 0 } // Elastic Notepad reference implementation

export interface Options
{
	spacing	: Spacing,
	tabSize	: number,
	indentSpaces	: number,
	ignoreSpaces	: boolean,
	indentAware	: boolean,
	alignNumbers	: boolean,
}
