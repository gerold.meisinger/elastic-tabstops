import { type CellsPerLine, spaces2, type Tabstop, type TextPerLine, Options } from './types'

const regStrHex	= '(?:[+-]?(?=0)0[xX][0-9a-fA-F]+)'
const regStrOct	= '(?:[+-]?(?=0)0[oO]?[0-7]+)'
const regStrBin	= '(?:[+-]?(?=0)0[bB][01]+)'
const regStrInt	= '(?:[+-]?\\d+)'
const regStrFloat	= '(?:[+-]?(?=.\\d))'
const regStrs = [regStrHex, regStrOct, regStrBin, regStrInt, regStrFloat]
export const intPartRegStr	= regStrs.map(s => '^' + s).join('|') // ^(?:[+-]?(?=0)0[xX][0-9a-fA-F]+)|^(?:[+-]?(?=0)0[oO]?[0-7]+)|^(?:[+-]?(?=0)0[bB][01]+)|^(?:[+-]?\d+)|^(?:[+-]?(?=.\d))
const intPartRegex = new RegExp(intPartRegStr)

export const getTextPerLine	= (text	: string	): TextPerLine	=> text.split(/\r?\n/)

export const getCellsPerLine	= (lines	: TextPerLine, indentSpaces = 0): CellsPerLine	=> {
	if (indentSpaces == 0) { return lines.map(line => line.split('\t')) }

	const match = " ".repeat(indentSpaces)
	const ret = lines.map(line => {
		let _indents	= 0
		let _c	= 0
		while (_c < line.length)
		{
			if	(line[_c] == '\t'	) { _indents += 1; _c += 1	}
			else if	(line.substring(_c, _c + indentSpaces) === match	) { _indents += 1; _c += indentSpaces	}
			else { break }
		}
		const indentCells = new Array(_indents).fill("")
		const stringCells = line.substring(_c).split('\t')
		return [...indentCells, ...stringCells]
	})
	return ret
}

/**
 * Find the maximum cell width of a column by iterating over runs of uninterrupted cell widths from {@link getCellWidths} and set them to the max value.
 * ```
 * [ [0, 1, 6]    [ [0, 3, 6]
 * , [0, 2]       , [0, 3]
 * , [0, 3, 4]    , [0, 3, 4]
 * , []        => , []
 * , [0, 4, 3]    , [0, 6, 3]
 * , [0, 5, 2]    , [0, 6, 3]
 * , [0, 6]       , [0, 6]
 * ]              ]
 * ```
 * @param cellWidthsPerLine
 * @returns
 */
export function getCellWidthsMax (cellWidthsPerLine: number[][], indentAware = true): number[][]
{
	const columnsMax	= Math.max(...cellWidthsPerLine.map(cellWidthsOfLine => cellWidthsOfLine.length))	// maximum number of columns over all lines

	const cellWidthsPerLineMaxed	: number[][] = cellWidthsPerLine.map(cellWidthsOfLine => new Array(cellWidthsOfLine.length).fill(0))
	const levels	: number[][] = cellWidthsPerLine.map(cellWidthsOfLine => Array.from({ length: cellWidthsOfLine.length }, (_, i) => Math.min(i, Math.max(cellWidthsOfLine.findIndex(w => w > 0), 0))))
	for (let column = 0; column < columnsMax; column++)
	{
		for (let line = 0; line < cellWidthsPerLine.length; line++)
		{
			if (cellWidthsPerLine[line].length <= column	) { continue } // skip lines until we reach the first cell of the current column
			if (cellWidthsPerLine[line][column] === 0 && indentAware	) { continue }

			const level = levels[line][column]

			// find the maximum of the current column by running lines forward
			let _widthMax	= -1
			let _lineRun	= line
			do
			{
				const width	= cellWidthsPerLine[_lineRun][column]
				_widthMax	= Math.max(width	, _widthMax)
				_lineRun	+= 1
			}
			// eslint-disable-next-line no-unmodified-loop-condition
			while (_lineRun < cellWidthsPerLine.length && cellWidthsPerLine[_lineRun].length > column && ((cellWidthsPerLine[_lineRun][column] !== 0 && levels[_lineRun][column] === level) || !indentAware)) // check for uninterrupted run

			// set maximum of current column to all lines
			for (let l = line; l < _lineRun; l++)
			{
				cellWidthsPerLineMaxed[l][column]	= _widthMax
			}
			line = _lineRun - 1
		}
	}

	return cellWidthsPerLineMaxed
}

/**
 * Split each textline at tab characters into cells and measure their widths with the last cell removed.
 * ```
 * 01             // width of these cells shouldn't    [ []
 * →   456789A123 // influence width of indentation    , [0]
 * →   →   aa↔           ddddd↔     gggggggg↔ jjj      , [0, 0,  2, 5, 8]
 * →   →   bbbbbbb↔      e↔         hhhhhh↔   kkk   => , [0, 0,  7, 1, 6]
 * →   →   cccccccccccc↔ fffffffff↔ iiii↔     lll      , [0, 0, 12, 9, 4]
 *                                                     ]
 * ```
 *
 * The last cell of each line is removed because:
 * 1. the width of the last cell has no influence on how to align the elastic tabstop in front of it
 * 2. it prevents indentation tabs to be aligned with the first line of a column block in {@link getCellWidthsMax}
 *
 * @param textPerLine
 * @param measureText
 * @returns
 */
export function getCellWidths<T> (text	: string	, indentSpaces?: number	, measureFunc? : (str: string) => T	): [T[][], [TextPerLine, CellsPerLine]]
export function getCellWidths<T> (textPerLine	: TextPerLine	, indentSpaces?: number	, measureFunc? : (str: string) => T	): [T[][], [TextPerLine, CellsPerLine]]
export function getCellWidths<T> (text	: string | TextPerLine	, indentSpaces : number = 0	, measureFunc  = (str: string) => str.length as unknown as T	): [T[][], [TextPerLine, CellsPerLine]]
{
	const textPerLine	= typeof text === 'string' ? getTextPerLine(text) : text
	const cellsPerLine	= getCellsPerLine(textPerLine, indentSpaces)
	const cellWidthsPerLine	= cellsPerLine	.map(cells	=> cells.map(measureFunc))
	return [cellWidthsPerLine, [textPerLine, cellsPerLine]]
}

/**
 * Calculate the {@link Tabstop} infos of a text tab-separated text for a monospaced font according to settings.
 * The indentation must not use mixed tabs and spaces.
 *
 * Example using `elasticSpacing=spaces-2` and `tabsize=4`:
 *
 * ```
 * →   →   aa→ ddddd→  gggggggg→   jjj
 * →   →   bbbbbbb→e→  hhhhhh→ kkk
 * →   →   cccccccccccc→   fffffffff→  iiii→   lll
 * ```
 *
 * becomes aligned accordingly:
 *
 * ```
 * →   →   aa↔           ddddd↔     gggggggg↔ jjj
 * →   →   bbbbbbb↔      e↔         hhhhhh↔   kkk
 * →   →   cccccccccccc↔ fffffffff↔ iiii↔     lll
 * ```
 * @param textPerLine
 * @param spacing
 * @param tabsize
 * @returns
 */
export function calcElasticTabstopsMonospaced (text: string | TextPerLine, options?: Partial<Options>): Tabstop[][]
{
	const { spacing = spaces2, tabSize = 4, indentSpaces = 0, ignoreSpaces = false, indentAware = false, alignNumbers = false } = options ?? {}

	const measureText	= (ignoreSpaces: boolean, str: string): number => (ignoreSpaces ? str.trimEnd() : str).length
	const measureInt	= (ignoreSpaces: boolean, str: string): number => {
		// numbers: +123, 3.14, .5, 1.23e10, 1.e-10
		const match	= (ignoreSpaces ? str.trimStart() : str).match(intPartRegex)
		const ret	= match != null ? match[0].length + 1 : ((ignoreSpaces ? str.trim() : str).length > 0 ? -1 : 0)
		return ret
	}

	const textPerLine	= typeof text === 'string' ? getTextPerLine(text) : text
	const [cellWidthsPerLine_, [, cellsPerLine]]	= getCellWidths(textPerLine, indentSpaces, s => measureText(ignoreSpaces, s))
	const cellWidthsPerLine	= cellWidthsPerLine_.map(cells => cells.slice(0, -1))
	const cellIntWidthsPerLine	= alignNumbers ? cellsPerLine.map(cells => cells.slice(1).map(s => measureInt(ignoreSpaces, s))) : []
	const cellRestWidthsPerLine	= alignNumbers ? cellWidthsPerLine_.map((cells, l) => cells.slice(1).map((w, c) => cellIntWidthsPerLine[l][c] >= 0 ? w - cellIntWidthsPerLine[l][c] + 2 : -1)) : []
	const cellWidthsMaxPerLine	= getCellWidthsMax(cellWidthsPerLine	, indentAware)
	const cellIntWidthsMaxPerLine	= getCellWidthsMax(cellIntWidthsPerLine	, indentAware)
	const cellRestWidthsMaxPerLine	= getCellWidthsMax(cellRestWidthsPerLine	, indentAware)

	const elasticTabstops: Tabstop[][] = []
	for (let l = 0; l < cellWidthsMaxPerLine.length; l++)
	{
		const elasticTabstopsOfLine : Tabstop[] = []
		const cellWidthsOfLine	= cellWidthsPerLine	[l]
		const cellWidthsMaxOfLine	= cellWidthsMaxPerLine	[l]

		let _isIndent	= true
		let _level	= 0
		let _charIdx	= 0	// index position of `\t` char in the textline string
		let _visPosEts	= 0	// visual position (=if text would be all spaces) of character with respect to tabsize
		let _visPosMod	= 0	// visual column position according to classic tab calculation (modulo)
		let _intPartPrev	= 0
		for (let c = 0; c < cellWidthsMaxOfLine.length; c++)
		{
			const intPartPrevMaxAdd	= alignNumbers ? (c > 0 && cellIntWidthsMaxPerLine[l][c - 1] > 0 ? cellIntWidthsMaxPerLine[l][c - 1] + cellRestWidthsMaxPerLine[l][c - 1] - 2 : 0) : 0
			const cellWidth	= cellWidthsOfLine	[c]
			const cellWidthMax	= Math.max(cellWidthsMaxOfLine[c], intPartPrevMaxAdd)

			const intPartAdd = alignNumbers ? (cellIntWidthsPerLine[l][c] > 0 ? cellIntWidthsMaxPerLine[l][c] - cellIntWidthsPerLine[l][c] : 0) : 0

			if (_isIndent)
			{
				// all tabs until the first max cell width > 0 are considered indentation tabs
				if (cellWidthMax === 0)
				{
					if (tabSize > 0)
					{
						const width = tabSize - _visPosEts % tabSize
						const ets: Tabstop	= { isIndent: true, level: _level, charIdx: _charIdx, visPosEts: _visPosEts, visPosMod: _visPosMod, widthEts: width, widthMod: width }
						elasticTabstopsOfLine.push(ets)
					}
					_charIdx	+= 1
					_visPosEts	+= tabSize
					_visPosMod	+= tabSize
					_level	+= 1
					continue
				}
				_isIndent = false
			}

			_charIdx	+= cellWidth
			_visPosEts	+= cellWidth
			_visPosMod	+= cellWidth

			const widthEts_	= Math.max(cellWidthMax + intPartAdd + spacing.pad, spacing.min) - cellWidth - _intPartPrev
			const widthEts	= spacing.mod === 0 ? widthEts_ : widthEts_ + (_visPosEts + widthEts_) % spacing.mod
			const widthMod	= tabSize > 0 ? tabSize - _visPosMod % tabSize : 0

			const ets: Tabstop	= { isIndent: false, level: _level, charIdx: _charIdx, visPosEts: _visPosEts, visPosMod: _visPosMod, widthEts, widthMod }
			elasticTabstopsOfLine.push(ets)

			_charIdx	+= 1
			_visPosEts	+= widthEts
			_visPosMod	+= widthMod
			_intPartPrev	= intPartAdd
		}

		elasticTabstops.push(elasticTabstopsOfLine)
	}
	return elasticTabstops
}
