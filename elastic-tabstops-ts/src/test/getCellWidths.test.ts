import assert from 'assert'
import 'mocha'

import { getCellWidths } from '../elastictabstops'
import { getTitle, loadTestcases } from './common'

suite('getCellWidths', function () {
	const testcases = loadTestcases(this.title)
	for (const testcase of testcases)
	{
		const { input, expected } = testcase
		const _title = getTitle(testcase)
		test(_title, () => { assert.deepEqual(getCellWidths(input.split('\n'))[0].map(cells => cells.slice(0, -1)), expected) })
	}
})
