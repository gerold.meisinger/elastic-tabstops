import assert from 'assert'
import 'mocha'

import { getCellsPerLine } from '../elastictabstops'
import { getTitle, loadTestcases } from './common'

suite('getCellsPerLine_indentSpaces', function () {
	const testcases = loadTestcases(this.title)
	for (const testcase of testcases)
	{
		const { input, expected } = testcase
		const title	= getTitle(testcase).replaceAll(" ", "·")
		const cells	= getCellsPerLine([input], 2)[0]
		const actual	= cells.findIndex(cell => cell.length > 0)
		test(title, () => assert.equal(actual, expected))
	}
})
