import assert from 'assert'
import 'mocha'

import { calcElasticTabstopsMonospaced } from '../elastictabstops'
import { getTitle, loadTestcases, options } from './common'

suite('numbers', function () {
	const testcases = loadTestcases(this.title)
	for (const testcase of testcases)
	{
		const title = getTitle(testcase)
		test(title, () => {
			const lines	= (testcase.input as string).split('\n')
			const etsss	= calcElasticTabstopsMonospaced(lines, { ...options, alignNumbers: true })
			const actual	= lines.map((line, l) => line.split('\t').map((cell, c) => cell + (c < etsss[l].length ? " ".repeat(etsss[l][c].widthEts) : '')).join(''))
			const expected	= testcase.expected.split('\n')
			assert.deepEqual(actual, expected)
		})
	}
})
