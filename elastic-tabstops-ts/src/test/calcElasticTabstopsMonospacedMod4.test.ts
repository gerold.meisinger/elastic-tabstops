import assert from 'assert'
import 'mocha'

import { calcElasticTabstopsMonospaced, getCellsPerLine, getCellWidths, getCellWidthsMax } from '../elastictabstops'
import { ElasticTabstopTupleFull, getTitle, loadTestcases, options } from './common'

suite('calcElasticTabstopsMonospacedMod4', function () {
	const testcases = loadTestcases(this.title)
	for (const testcase of testcases)
	{
		const title = getTitle(testcase)
		test(title, () => {
		const actual	= calcElasticTabstopsMonospaced(testcase.input.split('\n'), options)
		const expected	= testcase.expected.map((lines: ElasticTabstopTupleFull[]) => lines.map(([isIndent, level, charIdx, visPosEts, visPosMod, widthEts, widthMod]) => { return { isIndent, level, charIdx, visPosEts, visPosMod, widthEts, widthMod } }))
		assert.deepEqual(actual, expected)
		})
	}
})
