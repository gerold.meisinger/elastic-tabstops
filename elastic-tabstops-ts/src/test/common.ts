import { readFileSync } from 'fs'
import json5 from 'json5'

import { Options, spaces2 } from '../types'

export interface Testcase
{
	title?	: string
	input	: any
	expected	: any
	options?	: Partial<Options>
}

export type IsIndent	= boolean
export type Level	= number
export type CharIdx	= number
export type VisPosEts	= number
export type VisPosMod	= number
export type WidthEts	= number
export type WidthMod	= number

export type ElasticTabstopTuple	= [CharIdx, VisPosEts, VisPosMod, WidthEts, WidthMod]
export type ElasticTabstopTupleFull	= [IsIndent, Level, CharIdx, VisPosEts, VisPosMod, WidthEts, WidthMod]

export const options = { spacing: spaces2, tabsize: 4, ignoreSpaces: true, indentAware: false, alignNumbers: false }

export function getTitle (testcase: Testcase): string
{
	const ret = testcase.title ?? testcase.input.toString().replaceAll('\n', '↵').replaceAll('\t', '→')
	return ret
}

export function loadTestcases (filename: string): Testcase[]
{
	const json	= readFileSync(`../tests/${filename}.json5`, 'utf-8')
	const ret	= json5.parse(json).filter((v: string | Testcase): v is Testcase => typeof v !== 'string') // skip comments in json
	return ret
}
