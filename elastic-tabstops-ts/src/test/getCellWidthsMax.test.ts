import assert from 'assert'
import 'mocha'

import { getCellWidthsMax } from '../elastictabstops'
import { getTitle, loadTestcases, options } from './common'

suite('getCellWidthsMax', function () {
	const testcases = loadTestcases(this.title)
	for (const testcase of testcases)
	{
		const { input, expected } = testcase
		const actual	= getCellWidthsMax(input, options.indentAware)
		const title	= getTitle(testcase)
		test(title, () => { assert.deepEqual(actual, expected) })
	}
})
