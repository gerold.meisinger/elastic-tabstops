# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'Elastic Tabstops'
copyright = '2023, Gerold Meisinger'
author = 'Gerold Meisinger'

release = '0.1'
version = '0.1.0'

# -- General configuration

extensions = [
	  'myst_parser'
	, 'sphinx.ext.todo'
	]

intersphinx_mapping = {
	'python': ('https://docs.python.org/3/', None),
	'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'

# -- Options for EPUB output
epub_show_urls = 'footnote'

# -- Extensions
myst_enable_extensions = ["colon_fence"]

todo_include_todos=True
