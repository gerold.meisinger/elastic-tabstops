# Compatibility

## Default

### No elastic tabstops with tabsize=4 (`mod-4`)

```
/*
012301230123012301230123*/
→   var·a→  =·0
→   var·blub→   =·1
→   var·foobarbaz→  =·2
```

## Viewed as

### Elastic tabstops viewed as fixed tabs with tabsize=4 (`mod-4`)

```
/*                  ↓ aligned to mod-4 tab column
012301230123012301230123*/
→   var·a↔          =·0
→   var·blub↔       =·1
→   var·foobarbaz↔  =·2
```

### Elastic tabstops viewed as flexible tabs with at least 1 space

Supports code style rule "space before X".

```
→   var·a↔        =·0
→   var·blub↔     =·1
→   var·foobarbaz↔=·2
```

### Elastic tabstops viewed as flexible tabstops with 0 spaces

Supports code style rule "no space before X".

```
→   var·a↔       =·0
→   var·blub↔    =·1
→   var·foobarbaz=·2
                ╱╲ here ↔ is hidden between foobarbaz and =
```

## Stored as

### Elastic tabstops stored as tab character (default)

This is the way it is intended. Supports canonical form but in most text editors it will look like this:

```
→   var·a↔  =·1
→   var·blub↔   =·2
→   var·foobarbaz↔  =·3
```

### Elastic tabstops stored as aligned spaces

Most compatible as visual representation matches with elastic tabstops. May introduces a lot of unnecessary changes in diffs and conflict with linters (`eslint:no-multi-spaces`). The convention should always be 2 spaces for padding.

2 spaces for padding:

```
→   var·a··········=·1
→   var·blub·······=·2
→   var·foobarbaz··=·3
```

This mode makes use of the fact that in non-aligned code there are never 2 spaces (except for indentation). It requires language-aware features however as strings may still contain 2 spaces.

```
····var·strSpaces2··=·"  "··// everything should be allowed in strings
```

Columns of all empty cells may get lost if padding doesn't follow convention. Maybe 2 spaces were used for padding, maybe 4:

```
        ╲╱ empty column
→   aaa····ggg
→   bbb····hhh
→   ccc····iii
```

If we use only 1 space for padding it introduces too many ambiguities. First of all, we would align everything with only 1 space, which wasn't originally aligned.

```
→   var·a·=·0
→   var·b·=·1
→   var·c·=·2
```

We could add a restriction and only align if there are lines with 2 or more spaces above or below.

```
→   var·a·········=·1
→   var·blub······=·2
→   var·foobarbaz·=·3
→   /**·alignment·was not intended here */
                 ↑ should probably not be converted to elastic tabstop although it aligns
```

### Elastic tabstops stored as 2 spaces

Also supports canonical form but may conflict with linters (`eslint:no-multi-spaces`). The visual change is very minimal compared to "one space before X" styles.

```
→   var·a··=·1
→   var·blub··=·2
→   var·foobarbaz··=·3
```

This mode makes use of the fact that in non-aligned code there are never 2 spaces (except for indentation). It requires language-aware features however as strings may still contain 2 spaces.

```
····var·strSpaces2··=·"  "··// everything should be allowed in strings
```

## Experiments

### Elastic tabstops stored as vertical tab character

<https://github.com/nickgravgaard/ElasticNotepad/issues/2>

Also supports canonical form but may cause problems in various viewers.

```
→   var·a␋=·1
→   var·blub␋=·2
→   var·foobarbaz␋=·3
```

for testing (contains actual `\VT` character):

```
	var a= 1
	var blub= 2
	var foobarbaz= 3
```

Here is a list of [all ASCII C0 and C1 control codes](https://en.wikipedia.org/wiki/C0_and_C1_control_codes) which we could use instead:
```
dec	hex	abbr	unicode	ASCII	description
 0	00	NUL	␀	---	Null
 1	01	SOH	␁	""	Start of Heading
 2	02	STX	␂	""	Start of Text
 3	03	ETX	␃	""	End of Text
 4	04	EOT	␄	""	End of Transmission
 5	05	ENQ	␅	""	Enquiry
 6	06	ACK	␆	""	Acknowledge
 7	07	BEL	␇	""	Bell, Alert
 8	08	BS	␈	""	Backspace
 9	09	HT	␉	---	Character Tabulation, Horizontal Tabulation
10	0A	LF	␊	---	Line Feed
11	0B	VT	␋	""	Line Tabulation, Vertical Tabulation
12	0C	FF	␌	""	Form Feed
13	0D	CR	␍	---	Carriage Return
14	0E	SO	␎	""	Shift Out
15	0F	SI	␏	""	Shift In
16	10	DLE	␐	""	Data Link Escape
17	11	DC1	␑	""	Device Control One (XON)
18	12	DC2	␒	""	Device Control Two
19	13	DC3	␓	""	Device Control Three (XOFF)
20	14	DC4	␔	""	Device Control Four
21	15	NAK	␕	""	Negative Acknowledge
22	16	SYN	␖	""	Synchronous Idle
23	17	ETB	␗	""	End of Transmission Block
24	18	CAN	␘	""	Cancel
25	19	EM	␙	""	End of medium
26	1A	SUB	␚	""	Substitute
27	1B	ESC	␛	""	Escape
28	1C	FS	␜	""	File Separator
29	1D	GS	␝	""	Group Separator
30	1E	RS	␞	""	Record Separator
31	1F	US	␟	""	Unit Separator
```

*Here all quotes actually contain ASCII code control characters which may not show up in every text viewer!*

### Using mixed CR LF for EOFs to break or keep alignment

* Windows uses `\r\n` (CRLF) for `EOF`
* Unix uses `\n` (LF) for `EOF`
* Mac uses `\r` (CR) for `EOF`
* Some old OSes used `\n\r` for `EOF` (<https://en.wikipedia.org/wiki/Newline#Representation>)

Most editors are aware of this and already display them correctly. We could use mixed variants of `\r\n`, `\n\r`, `\n` and `\r` in the same file (ugh!) to store additional information about alignment across lines when there is no interrupting line. This would however break with most code style settings about using one `EOF` type.

```
→   function(→   a,␍
→   →   foo) {␍␊
→   →   var b→   = 0␍
→   →   var bar→   = 1␍␊
→   }
```
