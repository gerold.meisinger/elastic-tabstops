# Arguments

## Alignment with elastic tabstops

### Pro arguments for elastic tabstops

* more time is spent on reading code than writing code
* information design and visual cues for inherent structure and bugs
* looks more visually pleasing
* more code is aligned for easy vertical selection
* allow pretty printing characters (e.g. `⇒` instead of `=>`)

### Contra arguments against elastic tabstops for a project transition

* extra smart alignment produce fights with word processor => Disagreed, the behaviour is very predictable
* code looks different in other editors => Agreed, but can be mitigated with a "insert alignment spaces" option
* conflict with project settings like linters, formatters etc. => Agreed, this can be configured accordingly

### Contra arguments against elastic tabstops during ecosystem transition

* not widely supported => Agreed, but this can change.
* problematic in some languages => Agreed, implementations need more language awareness, but most can be solved with "indent-aware elastic tabstops"
* don't waste time manually(!) formatting code (*[Prettier: "stop all the on-going debates over styles"](https://prettier.io/docs/en/why-prettier.html)*) => Agreed. This requires to adapt an auto-formatter for elastic tabstops

### Contra arguments against elastic tabstops after ecosystem transition

* tabs will always be displayed differently in some editors => any argument which applies to tabs in general ("tabs for indentation") also applies to elastic tabstops
* classic elastic tabstops and indent-aware tabstops can never be differentiated automatically => you either have to choose which one you use or add file headers
* auto-formatters will never be smart enough to insert elastic tabstops for things like function call openings, function call parameters, similar one-liners etc. => either the auto-formatters removes them altogether (for those features) or ignores any manually inserted elastic tabstops (which could again result in style edit wars)

## Rendering with proportional fonts

### Pro arguments for proportional fonts

* narrow and wide characters look more pleasing
* different text weights look more pleasing
* allow various styles for different kinds of code (like sans for code, serif for strings, bold for keywords, italic for comments etc.)
* optical cues for typos

```
<div><span class="highlight">M</span></div>
<div><spam class="highlight">M</spam></div>
```

### Contra arguments against proportional fonts

* <https://stackoverflow.com/questions/218623/why-use-monospace-fonts-in-your-ide>
* difficult to support by text editor
* vertical selection is unreliable with unaligned text

## External discussions

* <https://tibleiz.net/code-browser/elastic-tabstops.html>
* <https://forum.sublimetext.com/t/elastic-tabs/128/47>
* <https://sublimetext.userecho.com/communities/1-general#module_18>
* <https://tibleiz.net/code-browser/elastic-tabstops.html>
* <https://lobste.rs/s/1ugcr4/elastic_tabstops_better_way_indent_align>
* <https://softwareengineering.stackexchange.com/questions/137290/what-are-the-drawbacks-of-elastic-tabstops>
* <https://news.ycombinator.com/item?id=17643629>
* <https://news.ycombinator.com/item?id=333626>
* <https://news.ycombinator.com/item?id=31477853>
* <https://slashdot.org/story/06/07/03/1820235/elastic-tabstops-an-end-to-tabs-vs-spaces>
* <https://wincent.dev/blog/tabs-vs-spaces-or-spaces-vs-tabs-and-elastic-tabstops>
* <https://www.reddit.com/r/programming/comments/8mebl/ive_been_waiting_for_this_for_ages_is_this_the>
* <https://www.reddit.com/r/programming/comments/77dpp/elastic_tabstops_a_better_way_to_indent_and_align>
* <https://www.reddit.com/r/emacs/comments/364ovl/elastic_tabstops_a_better_way_to_indent_and_align>
* <https://www.reddit.com/r/programming/comments/6ase82/elastic_tabstops_a_better_way_to_indent_and_align>
* <https://groups.google.com/g/gnu.emacs.help/c/4kNcl1xNqRc/m/5ww75oA6sE4J>
* <https://groups.google.com/g/vim_dev/c/oKwBgbh2yL4>
* <https://intellindent.info/seriously>
* <https://github.com/crystal-lang/crystal/issues/1682>
* <https://forum.lazarus.freepascal.org/index.php?topic=31083.0>
* <https://linux.samba.narkive.com/3YeoTwLS/clug-elastic-tabstops>
* <https://forums.ultraedit.com/elastic-tabstops-t6741.html>
