================
Elastic Tabstops
================

*A better way to indent and align code*

.. toctree::
	:maxdepth: 2
	:caption: Contents

	intro
	arguments
	editors
	implementation
	discussion
	contribute

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
