# Introduction

Original proposal of [Elastic Tabstops by Nick Gravgaard](https://nickgravgaard.com/elastic-tabstops):

![](./images/columnblocks_coloured.gif)

*This animated diagram shows how the elastic tabstops mechanism aligns text. A tab character is represented as a vertical line. Each cell ends with a tab character. A column block is a run of uninterrupted vertically adjacent cells. A column block is as wide as the widest piece of text in the cells it contains or a minimum width (plus padding). Text outside column blocks is ignored.* -- Nick Gravgaard

This document wants to collect all the arguments, implementation details and issues, and existing solutions.
