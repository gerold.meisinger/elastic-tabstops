# Testcases

## Full testcase

* Don't make columns smaller than mod-4 otherwise it's hard to distinguish between regular tabstops and elastic tabstops
* Make testcases were every cell in a column is
	* a) one smaller than mod-4 and one larger than mod-8: makes sure the whole column actually aligns with the largest cell and columns to the right are shifted to the right
	* b) smaller than mod-4: makes sure `elasticTabsize=spaces-n` are different than mod-4
	* c) larger than mod-8: makes sure if cells below are elastic or just use regular tabstops
* Make the first cell the smallest to make sure  max cell width calculation works
* Use different content size in every cell so it's easier to comprehend in debugger
* If column blocks are connected with tab-only lines make the max width of the upper and lower column very different from each other.

:::{todo}
add full design thoughts
:::

## Thoughts on mod-8

```
0123456789A123456789B123456789C123456789D  // visual column position
|       |       |       |       |       |  // tabstops mod-8
aaaaaaaa→       dddddddd→       gggggggg
bbbbbbbb→       eeeeeeee→       hhhhhhhh
cccccccc→       ffffffff→       iiiiiiii
```

```
0123456789A123456789B123456789C123456789D  // visual column position
|       |       |       |       |       |  // tabstops mod-8
a→      d→      g
b→      e→      h
c→      f→      i
```

```
0123456789A123456789B123456789C123456789D  // visual column position
|       |       |       |       |       |  // tabstops mod-8
aaaaaaaaaaaa→   dddddddddddd→   ggggggggg
bbbbbbbbbbbb→   eeeeeeeeeeee→   hhhhhhhhh
cccccccccccc→   ffffffffffff→   iiiiiiiii
```

```
0123456789A123456789B123456789C123456789D  // visual column position
|       |       |       |       |       |  // tabstops mod-8
aaaa→   dddd→   gggg
bbbb→   eeee→   hhhh
cccc→   ffff→   iiii
```

```
0123456789A123456789B123456789C123456789D  // visual column position
|       |       |       |       |       |  // tabstops mod-8
aa→     dddd→   ggggggg
bbbbbbb→        e→      hhhhh
cccccccccccc→   fffffffffff→    iii
```

```
0123456789A123456789B123456789C123456789D  // visual column position
|   |   |   |   |   |   |   |   |   |   |  // tabstops mod-4
aa→ dddd→   ggggggg
bbbbbbb→e→  hhhhh
cccccccccccc→   fffffffffff→iii
```

```
0123456789A123456789B123456789C123456789D  // visual column position
aa↔           dddd↔        ggggggg
bbbbbbb↔      e↔           hhhhh
cccccccccccc↔ fffffffffff↔ iii
```

:::{todo}
rework according to conventions
:::

## Types

**Tabs**

```
aaa
→   bbb
→   →   ccc
```

**Four spaces**

```
ddd
····eee
········fff
```

**Mixed tabs and spaces**

```
→   ggg
    →   hhh
→   ····iii
→   ····→   jjj
····→   ····kkk
```

:::{todo}
more weird examples with mixed tabs and spaces
:::

**One cell per line**

`rectangle 2x3`

```
→   a↔   111
→   bbb↔   111
→   ccccc↔   111
```

for testing (contains actual `\t` characters):

```
	a	111
	bbb	111
	ccccc	111
```

**Two cells per line**

`rectangle 3x3`

```
→   d↔     1↔     222
→   eee↔   111↔   222
→   fffff↔ 11111↔ 222
```

for testing (contains actual `\t` characters):

```
	d	1	222
	eee	111	222
	fffff	11111	222
```

**Different number of cells per line**

`triangle bottom-left`

```
→   g↔     111
→   hhh↔   111↔ 222
→   iiiii↔ 111↔ 222↔ 333
```

for testing (contains actual `\t` characters):

```
	g	111
	hhh	111	222
	iiiii	111	222	333
```

:::{todo}
`triangle top-left`
:::

**Empty cells in the middle of the line**

`diagonal down` `diagonal up`

```
→   j↔     111
→   kkk↔   ↔ 222
→   lllll↔ ↔ ↔ 333

→   mmmmm↔ ↔ ↔ 333
→   nnn↔   ↔ 222
→   o↔     111
```

for testing (contains actual `\t` characters):

```
	j	111
	kkk		222
	lllll			333

	mmmmm			333
	nnn		222
	o	111
```

**Empty cells on the start(!) of the line**

`triangle top-right` `triangle bottom-right`

```
→   mmm↔ 111↔ 222
→   ↔    111↔ 222
→   ↔    ↔    222

→   ↔      ↔    222
→   ↔      111↔ 222
→   nnnnn↔ 111↔ 222
```

for testing (contains actual `\t` characters):

```
	mmm	111	222
		111	222
			222

			222
		111	222
	nnnnn	111	222
```

**Extended column block**

```
→   a↔     1↔     222
→   bbb↔   111↔   222
→   ccccc↔ 11111↔ 222
→   d
→   e↔   1↔   222
→   ff↔  11↔  222
→   ggg↔ 111↔ 222
```

for testing (contains actual `\t` characters):

```
	a	1	222
	bbb	111	222
	ccccc	11111	222
	d
	e	1	222
	ff	11	222
	ggg	111	222
```

## External testcases

* [Elastic Notepad](https://github.com/nickgravgaard/ElasticNotepad/blob/master/app/test/src/elasticTabstopsTests.scala) only covers spaces and tabs
* [Visual Studio - Always Aligned](https://github.com/nickgravgaard/AlwaysAlignedVS/blob/master/ElasticTabstopsConverterTest/ElasticTabstopsConverterTest.cs)
* [Visual Studio Code - Smart Column Indentor](https://github.com/lmcarreiro/smart-column-indenter/blob/master/test/indenter.ts)
