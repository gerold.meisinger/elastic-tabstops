# Editors

:::{todo}
Adopt elastic tabstop implementations from the following sites:
* <https://nickgravgaard.com/elastic-tabstops>
* <https://stackoverflow.com/questions/28652/elastic-tabstop-editors-and-plugins>
* <https://intellij-support.jetbrains.com/hc/en-us/community/posts/206330979-Elastic-tabstops>
* <https://github.com/hax/atom-elastic-tabstops>
:::

## Mainline support

* [Elastic Notepad](https://github.com/nickgravgaard/ElasticNotepad) (by Nick Graavgard)

## Editor plugins

* Visual Studio Code
	* [Elastic Tabstops Lite Redux](https://marketplace.visualstudio.com/items?itemName=gerold-meisinger.elastic-tabstops-lite-redux) (from the author of this doc)
	* [Elastic Tabstops Lite](https://marketplace.visualstudio.com/items?itemName=isral.elastic-tabstops-lite)
	* [Elastic Tabstops Mono](https://marketplace.visualstudio.com/items?itemName=isral.elastic-tabstops-mono)
* Notepad++
	* [ColumnsPlusPlus](https://github.com/Coises/ColumnsPlusPlus)
	* [Elastic Tabstops (mariusv)](https://github.com/mariusv-github/ElasticTabstops)
	* [Elastic Tabstops (dail8859)](https://github.com/dail8859/ElasticTabstops)
	 	* [broken as of 2022-04-21 (last checked Apr 2023)](https://github.com/dail8859/ElasticTabstops/issues/31#issuecomment-1242974880)
	 	* [fixed 2022-09-11](https://github.com/dail8859/ElasticTabstops/issues/31#issuecomment-1242974880)
* Visual Studio
	* [Always Aligned](https://github.com/nickgravgaard/AlwaysAlignedVS) (by Nick Graavgard)
	* [only works for Visual Studio 2017 (last checked Apr 2023)](https://github.com/nickgravgaard/AlwaysAlignedVS/issues/16) => [download Visual Studio 2017 Community](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15)
* Scintilla
	* [Elastic Tabstops](https://github.com/nickgravgaard/ElasticTabstopsForScintilla) (by Nick Graavgard)
* Atom/Pulsar
	* [Elastic tabstops](https://web.pulsar-edit.dev/packages/elastic-tabstops)

## Feature requests

* [Visual Studio Code - Support Elastic Tabstops](https://github.com/microsoft/vscode/issues/3932)
	* [Monaco - Add support for elastic tabstops](https://github.com/microsoft/monaco-editor/issues/1973)
	* [Virtual space is not implemented](https://github.com/microsoft/vscode/issues/13960)
	* [Tab characters are the wrong width with proportional fonts](https://github.com/microsoft/vscode/issues/108755)
	* [Size of units in the letterspacing property of a tab character are erroneously scaled up](https://github.com/microsoft/vscode/issues/115307#issuecomment-769783361)
* [Jetbrains - Support elastic tabstops](https://youtrack.jetbrains.com/issue/IDEABKL-5748)
	* [discussion 1](https://intellij-support.jetbrains.com/hc/en-us/community/posts/206381789-elastic-tabstops)
	* [discussion 2](https://intellij-support.jetbrains.com/hc/en-us/community/posts/206330979-Elastic-tabstops)
	* [discussion 3](https://intellij-support.jetbrains.com/hc/en-us/community/posts/360010219720-How-can-I-set-widths-of-individual-tab-characters-with-a-plugin-)
* [Zed](https://github.com/zed-industries/zed/issues/4961)
* [Kate](https://bugs.kde.org/show_bug.cgi?id=130229)
* [Lapce](https://github.com/lapce/lapce/issues/2373)
* [Neovim](https://github.com/neovim/neovim/issues/1419)
* [Helix (closed)](https://github.com/helix-editor/helix/issues/730)

## Libraries

* Python
	* [elastic-tabstops-py](https://github.com/nick-gravgaard/elastic-tabstops-py)
* Javascript
	* [etab](https://github.com/hax/etab)

## Commandline interfaces

* [Elastic Tabstops CLI (Typescript)](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/tree/main/elastic-tabstops-ts-cli) (from the author of this doc)
* [tabwriter (Rust)](https://github.com/BurntSushi/tabwriter)
* [etst (C++)](https://github.com/sbuller/etst)
* [tabwriter plugin for gofmt (Go)](https://pkg.go.dev/text/tabwriter)

## Implementations

* [Elastic Notepad - elasticTabstops.scala](https://github.com/nickgravgaard/ElasticNotepad/blob/master/app/src/elasticTabstops.scala)
	* [Online demo on Scala Scastie](https://scastie.scala-lang.org/KI0z14XATkq1I6Bqa4ZRIA)
* [Notepad++ - ColumnsPlusPlus - ColumnsPlusPlus.cpp](https://github.com/Coises/ColumnsPlusPlus/blob/master/src/ColumnsPlusPlus.cpp)
* [Visual Studio - Always Aligned - ElasticTabstopsConverter.cs](https://github.com/nickgravgaard/AlwaysAlignedVS/blob/master/AlwaysAlignedVS/ElasticTabstopsConverter.cs)
* [Scintilla - Elastic Tabstops - ElasticTabstops.cpp](https://github.com/nickgravgaard/ElasticTabstopsForScintilla/blob/master/ElasticTabstops.cpp)

## Implementation tutorials

* [This document@Implementation](implementation.md)
* [observablehq.com@shaunlebron/elastic-tabstops](https://observablehq.com/@shaunlebron/elastic-tabstops)
* [dpwright.com - Elastic Tabstops for Haskell Pandoc](https://dpwright.com/posts/2015/05/02/generating-this-website-part-6-elastic-tabstops)

## Related plugins

* Visual Studio Code
	* [TSV](https://github.com/chtenb/vscode-tsv)
	* [Smart Column Indentor](https://github.com/lmcarreiro/smart-column-indenter)
* Jetbrains
	* [Smart Align](https://plugins.jetbrains.com/plugin/13903-smart-align): vertically aligns code by `=`, `:` or `,` characters
* Emacs
	* [Smart Tabs](https://www.emacswiki.org/emacs/SmartTabs)
* ACE
	* [Elastic Tabstops Lite Extension](https://github.com/ajaxorg/ace/blob/master/src/ext/elastic_tabstops_lite.js)

## Proportional Fonts for Programming

* [Input](http://input.fontbureau.com/info/)
* [Iosevka Aile Code](https://github.com/VulumeCode/Iosevka-Aile-Code)
* [Monaspace](https://github.com/githubnext/monaspace)
