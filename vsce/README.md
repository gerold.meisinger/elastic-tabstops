# Elastic Tabstops Lite Redux - Establishing Alignment Assignment

[Elastic Tabstops](https://nick-gravgaard.com/elastic-tabstops) are a simple yet powerful idea to keep code automatically aligned by using tabs to denote flexible alignment markers. Every text which is preceded with a tab is always left-aligned to the widest cell of the same column block. The classic tab functionality which only aligns to the next fixed modulos of a column is overridden. Elastic tabstops are very predictable and - once inserted - always keep the code aligned. They don't rely on super-smart alignment rules, don't insert alignment spaces into your file and don't require to manually reformat on every text change. Your diffs will stay clean and they don't interfere with programming languages.

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/elastictabstops.gif)

If you want know more about elastic tabstops, the arguments, implementation and how it affects code style see this [documentation](https://elastic-tabstops.readthedocs.io)

## Smart Align

You can insert elastic tabstops manually by just pressing `[Tab]` like you are used to anywhere you want. If you want to automatically insert tabs on the current block press `Shift+Alt+-` to insert them on all lines with the same indent level at the current cursor position (by predefined delimiters). If you highlight multiple lines only these lines will be aligned. If you highlight a character (or word) only it will be used as a delimiter on the lines of the same indent level. You can call smart align multiple times on already aligned lines to align more aggressively (or remove it altogether):

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/smartalign.gif)

1. level: align by assignments and comments (`=`, `:`, `+=`, `//` etc.)
2. level: align by separators (`,`, `|`, `(` etc.)
3. level: right-align numbers
4. level: show notification "removal imminent"
5. level: remove elastic tabstops

# Known issues

**Warning: You have to choose your poison! ... for now**

The way this extension works is by either inserting spaces in the editor view or changing the visual width of tab characters but each has its' own downside ([see discussion here](https://github.com/microsoft/vscode/issues/3932#issuecomment-2442445991)):

* If you set `alignmentSpaces=true` (recommended) the extension will insert spaces for alignment in the editor view and this will sometimes interfere with the cursor position while you write (use a higher `timeout` value).
* If you set `alignmentSpaces=false` the extension uses `letter-spacing` decoration for alignment and this will confuse Vscode about the character position and breaks cursor movement. The cursor will jump left and right randomly while you move up or down and vertical selections are shifted left and right.

Ultimately what we want is to use absolute positioning by pixel values for alignment and have Vscode take font-metrics and absolute character positions into account for cursor movement. But this is not supported by Vscode yet. So unfortunately this extension is *experimental* for now but I think it does work quite well for the most part.

If you think elastic tabstops are a great idea [please vote for real elastic tabstops support in Vscode!](https://github.com/microsoft/vscode/issues/3932)

# Settings

* `elasticTabstops.spacing` padding, minimum width and modulos setting of elastic tabstops

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/spacing.gif)

* `elasticTabstops.alignmentSpaces` use spaces for displaying alignment instead of `letter-spacing` decoration
* `elasticTabstops.storeSpaces` store alignment spaces in the file

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/alignmentSpaces.gif)

* `elasticTabstops.indentAware` alignment blocks also take indent level into account instead of just the tabstop number

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/indentAware.gif)

* `elasticTabstops.alignNumbers` right-align numbers on their integer part

![animation](https://gitlab.com/gerold.meisinger/elastic-tabstops/-/raw/main/vsce/imgs/alignNumbers.gif)

* `elasticTabstops.alignOnLoad` immediately appyly elastic tabstops view when document is loaded but will mark the document dirty even if nothing has changed so far (not recommended).

* `elasticTabstops.timeout` milliseconds before applying the alignment

In your workspace you probably want to set the following:
* `"editor.quickSuggestionsDelay": 500` (or more)
* reconsider `"editor.insertSpaces"` and `"editor.detectIndentation"`
* turn off any auto-formatter
* for linters see [Docs#Linters](https://elastic-tabstops.readthedocs.io/discussion.html#linters)

Special thanks to [Nick Gravgaard](https://nick-gravgaard.com) for the idea and [Isral](https://github.com/isral/elastic_tabstops_mono.vsce) for inspiring this extension.
