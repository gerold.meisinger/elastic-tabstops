import { Spacing, spaces2, expandToSameIndent, CharIdx, findDelimiterPositions, calcElasticTabstopsMonospaced, getRangesOfTabsWithSpaces, Tabstop } from 'elastic-tabstops'
import { commands, DecorationRangeBehavior, type ExtensionContext, Position, Range, TextDocument, TextEdit, type TextEditor, TextEditorDecorationType, TextEditorEdit, window, workspace, WorkspaceEdit } from 'vscode'

const debug = true
const name = "elasticTabstops"

interface Configuration
{
	spacing	: Spacing
	timeout	: number
	alignmentSpaces	: boolean
	storeSpaces	: boolean
	indentAware	: boolean
	alignNumbers	: boolean
	alignOnLoad	: boolean
}

const defaultConf: Configuration =
{
	spacing	: spaces2,
	timeout	: 1000,
	alignmentSpaces	: true,
	storeSpaces	: true,
	indentAware	: true,
	alignNumbers	: true,
	alignOnLoad	: false,
}

let _conf	: Configuration	= defaultConf
let _activeEditor	: ElasticEditor | undefined
let _smartAlignNum	: number
const delimitersAssignment =
[ // sorted by precedence(!)
	'>>>='	, '<<='	, '>>='	,	// compound assignment (shift)
	'&='	, '|='	, '^='	, // '!=',	// compound assignment (binary)
	'%='	, '//='	, '**='	,	// compound assigment (special)
	'+='	, '-='	, '*='	, '/=',	// compound assignment (arithemtic)
	'='	, ':'	// standard assigment
]
const delimitersComment	= [ '//', '/*', '(*', '<!--']
const delimitersSeparator	= [',', ';', '|', '(', '{', '[', ')', '}', ']']

function loadConfiguration (): Configuration
{
	const getConfiguration	= workspace.getConfiguration(name).get

	const conf: Configuration =
	{
		spacing	: getConfiguration<Spacing	>('spacing'	, defaultConf.spacing	),
		timeout	: getConfiguration<number	>('timeout'	, defaultConf.timeout	),
		alignmentSpaces	: getConfiguration<boolean	>('alignmentSpaces'	, defaultConf.alignmentSpaces	),
		storeSpaces	: getConfiguration<boolean	>('storeSpaces'	, defaultConf.storeSpaces	),
		indentAware	: getConfiguration<boolean	>('indentAware'	, defaultConf.indentAware	),
		alignNumbers	: getConfiguration<boolean	>('alignNumbers'	, defaultConf.alignNumbers	),
		alignOnLoad	: getConfiguration<boolean	>('alignOnload'	, defaultConf.alignOnLoad	),
	}

	return conf
}

function log(site: string, msg = ""): void
{
	if (!debug) { return }
	console.log(`${name}.${site}${msg.length > 0 ? ': ' + msg : ''}`)
}

function warn(site: string, msg: string): void
{
	if (!debug) { return }
	console.warn(`${name} (${site}): ${msg}`)
}

export function activate (context: ExtensionContext): void
{
	const site	= 'activate'
	_conf	= loadConfiguration()

	if (window.activeTextEditor != null)
	{
		log(site, '\tfirst active elastic editor')
		_activeEditor = new ElasticEditor(window.activeTextEditor)
		if (_conf.alignOnLoad)
		{
			_activeEditor.updateDocument(_conf)
		}
	}

	workspace.onDidChangeConfiguration(event => {
		log("onDidChangeConfiguration")
		const confOld = _conf
		_conf = loadConfiguration()
		if (_activeEditor == null	)	{ return }
		if (_conf.alignOnLoad)
		{
			_activeEditor.updateDocument(_conf)
		}
	}, null, context.subscriptions)
	window.onDidChangeTextEditorOptions(event => {
		const site = "onDidChangeTextEditorOptions"
		log(site)
		if (_activeEditor == null	)	{ return }
		if (_activeEditor.editor !== event.textEditor	)	{ return }
		_activeEditor.updateDocument(_conf)
	}, null, context.subscriptions)
	window.onDidChangeActiveTextEditor(activeEditor => {
		const site = "onDidChangeActiveTextEditor"
		log(site)
		if (_activeEditor != null)
		{
			log(site, '\tdispose old active editor')
			_activeEditor.dispose()
			_activeEditor = undefined
		}

		if (activeEditor == null) { return }

		log(site, '\tcreated new active editor')
		_activeEditor = new ElasticEditor(activeEditor)
		if (_conf.alignOnLoad)
		{
			_activeEditor.updateDocument(_conf)
		}
	}, null, context.subscriptions)
	window.onDidChangeVisibleTextEditors(editors => {
		//log("onDidChangeVisibleTextEditors")
	});
	workspace.onDidOpenTextDocument(document => {
		log("onDidOpenTextDocument", "do nothing")
	}, null, context.subscriptions)
	workspace.onDidChangeTextDocument(async event => {
		const site = "onDidChangeTextDocument"
		log(site, _activeEditor?._isUnformatted + "")
		const { document, contentChanges } = event
		if (document == null || document.isClosed	) { return }
		if (_activeEditor == null	) { return }
		if (_activeEditor.editor.document !== document	) { return }
		if (event.reason != null	) { return }	// don't align on undo/redo calls
		if (contentChanges.length === 0	) { return }	// sometimes empty events are fired(?)
		if (_activeEditor._isUnformatted	) { return }

		_activeEditor.triggerUpdateDocument(_conf)
	}, null, context.subscriptions)
	workspace.onDidCloseTextDocument(document => {
		const site = "onDidCloseTextDocument"
		log(site)

		if (_activeEditor == null	) { return }
		if (_activeEditor.editor.document !== document	) { return }

		_activeEditor.dispose()
		_activeEditor = undefined
		log(site, "\tactive elastic editor disposed")
	}, null, context.subscriptions)
	workspace.onWillSaveTextDocument(event => {
		const site = "onWillSaveTextDocument"
		log(site)
		const { document } = event
		if (_activeEditor == null	) { return }
		if (_activeEditor.editor.document !== document	) { return }
		if (document == null || document.isClosed	) { return }

		if (_conf.storeSpaces !== _conf.alignmentSpaces)
		{
			log(site, "\tprepare file for save")

			event.waitUntil(_activeEditor.editor.edit(builder => {
				const document = _activeEditor?.editor.document
				if (document == null || document.isClosed) { warn(site, 'document became invalid during edit'); return; }
				const textedits = replaceElasticTabstops(document, _conf, _conf.storeSpaces)
				textedits.forEach(te => builder.replace(te.range, te.newText + "\t"))
			}, { undoStopBefore: false, undoStopAfter: false })
				.then(() => log(site, "\tdone"), reason => warn(site, reason))
			)
		}
	}, null, context.subscriptions)
	workspace.onDidSaveTextDocument(document => {
		const site = "onDidSaveTextDocument"
		log(site)
		if (_activeEditor == null	) { return }
		if (_activeEditor.editor.document !== document	) { return }
		if (document == null || document.isClosed	) { warn(site, 'document became invalid after save'); return; }

		if (_conf.storeSpaces !== _conf.alignmentSpaces)
		{
			_activeEditor._isUnformatted = true
			_activeEditor.triggerUpdateDocument(_conf, { reset: true })
		}
	}, null, context.subscriptions)
	window.onDidChangeTextEditorSelection(event => {
		const site = "onDidChangeTextEditorSelection"
		log(site)
		if (_activeEditor == null	) { return }
		if (_activeEditor.editor !== event.textEditor	) { return }

		if	(_activeEditor._isUnformatted)
		{
			_activeEditor.updateDocument(_conf)
		}
		_smartAlignNum = 0
	})
	context.subscriptions.push(commands.registerTextEditorCommand(`${name}.smartAlign`, smartAlign))
}

function smartAlign(editor: TextEditor, edit: TextEditorEdit)
{
	const site = "smartAlign"
	log(site)
	const { document } = editor
	if (document == null || document.isClosed	) { return }
	if (_activeEditor == null	) { return }
	if (_activeEditor.editor.document !== document	) { return }

	const { selection }	= editor
	const startPos	= selection.start
	const endPos	= selection.end
	const isSameLine	= startPos.line === endPos.line
	const isSelection	= isSameLine && startPos.character !== endPos.character

	const textPerLine	= getTextPerLine(document)
	const [startLine, endLine]	= isSameLine ? expandToSameIndent	(textPerLine, startPos.line) : [startPos.line, endPos.line]
	const selectedLines	= textPerLine.slice(startLine, endLine + 1)

	let _charIdxsPerLine: CharIdx[][] = []
	if (!isSelection)
	{
		if (_smartAlignNum === 0) { _charIdxsPerLine = findDelimiterPositions	(selectedLines, [...delimitersAssignment, ...delimitersComment]	); window.showInformationMessage('smart align assignments and comments') }
		if (_smartAlignNum === 1) { _charIdxsPerLine = findDelimiterPositions	(selectedLines, delimitersSeparator	); window.showInformationMessage('smart align separators') }
		if (_smartAlignNum === 2) // align numbers
		{
			if (_conf.alignNumbers)
			{
				const regex = new RegExp(/[+-]?(?:0[xX][0-9a-fA-F]+|0[oO][0-7]+|0[bB][01]+|\d*\.?\d+(?:[eE][+-]?\d+)?)/g)
				let _match: RegExpExecArray | null
				for (const line of selectedLines)
				{
					const _charIdxs = []
					while ((_match = regex.exec(line)) != null)
					{
						if (_match[0] === "") { regex.lastIndex++; continue }
						_charIdxs.push(_match.index)
					}

					_charIdxsPerLine.push(_charIdxs)
				}
				window.showInformationMessage('smart align numbers')
			}
			else
			{
				_smartAlignNum += 1
			}
		}
		if (_smartAlignNum === 3) { window.showInformationMessage('smart align: removal imminent!') }
		if (_smartAlignNum === 4)
		{
			if (_activeEditor != null)
			{
				_activeEditor.editor.edit(builder => {
					const document = _activeEditor?.editor.document
					if (document == null || document.isClosed) { warn(site, 'document became invalid during edit'); return; }
					const textedits = replaceElasticTabstops(document, _conf, false)
					textedits.forEach(te => builder.replace(te.range, " "))
				})
				.then(() => window.showInformationMessage('elastic tabstops removed'))
			}
			_smartAlignNum = -1
		}
		_smartAlignNum += 1
	}
	else
	{
		_charIdxsPerLine = findDelimiterPositions	(selectedLines, [textPerLine[startPos.line].slice(startPos.character, endPos.character)])
		_smartAlignNum = 0
	}

	if (_charIdxsPerLine.length === 0) { return }

	_activeEditor.editor.edit(builder => {
		const document = _activeEditor?.editor.document
		if (document == null || document.isClosed) { warn(site, 'document became invalid during edit'); return; }
		for (let l = 0; l < _charIdxsPerLine.length; l++)
		{
			const charIdxs = _charIdxsPerLine[l]
			for (let i = 0; i < charIdxs.length; i++)
			{
				const charIdx	= charIdxs[i]
				const originalLine	= l + startLine
				builder.insert(new Position(originalLine, charIdx), '\t')
			}
		}
	})
	.then(() => _activeEditor?.updateDocument(_conf))
}

export function deactivate ()
{
	_activeEditor?.dispose()
}

// function disable()
// {
//	if (_conf.alignmentSpaces)
//	{
//		const edit = new WorkspaceEdit()

//		for (const document of workspace.textDocuments)
//		{
//			if (document == null || document.isClosed) { continue }
//			if (document.uri.scheme !== 'file') { continue }

//			const textedits = replaceElasticTabstops(document, _conf, false)
//			textedits.forEach(te => edit.replace(document.uri, te.range, te.newText + "\t"))
//		}
//		workspace.applyEdit(edit)
//	}
// }

function getTextPerLine(document: TextDocument | undefined): string[]
{
	if (document == null || document.isClosed) { return [] }
	const ret = Array.from({ length: document.lineCount }, (_, idx) => document.lineAt(idx).text)
	return ret
}

function replaceElasticTabstops(document: TextDocument, conf: Configuration, addSpaces = true): TextEdit[]
{
	const _ret: TextEdit[] = []

	const textPerLine	= getTextPerLine(document)
	const tabstops	= calcElasticTabstopsMonospaced(textPerLine, { ...conf, tabSize: 0, ignoreSpaces: true })
	const ranges	= getRangesOfTabsWithSpaces(textPerLine)
	const replacer	= addSpaces ? (ets: Tabstop) => " ".repeat(Math.max(ets.widthEts - conf.spacing.pad, 0)) : () => ""
	for (const [l, rangesOfLine] of ranges.entries())
	{
		for (const [i, range] of rangesOfLine.entries())
		{
			if (i >= tabstops[l].length) { continue }
			const ets = tabstops[l][i]
			const str = replacer(ets)

			const [pos0	, pos1	] = range
			const [line0	, char0	] = pos0
			const [line1	, char1	] = pos1
			if (char0 == char1 - 1)
			{
				const start	= new Position(line0, char0)
				const end	= new Position(line0, char1)
				const insert	= new TextEdit(new Range(start, end), str)
				_ret.push(insert)
			}
			else
			{
				const start	= new Position(line0	, char0)
				const end	= new Position(l	, char1)
				const replace	= new TextEdit(new Range(start, end), str)
				_ret.push(replace)
			}
		}
	}

	return _ret
}

interface IDecoration
{
	type	: TextEditorDecorationType
	ranges	: Range[]
}

function formatTabstopPositions (elasticTabstops: Tabstop[][], pad: number): IDecoration[]
{
	const decorations = new Map<string, IDecoration>()
	for (let l = 0; l < elasticTabstops.length; l++)
	{
		for (let c = 0; c < elasticTabstops[l].length; c++)
		{
			const { isIndent, charIdx, widthEts, widthMod } = elasticTabstops[l][c]
			if (isIndent) { continue }

			const key	= pad == -1 ? `${	widthEts}/${	widthMod}`	: `${	pad}/${	widthMod}`
			const stretchFactor	= pad == -1 ?	widthEts /	widthMod	:	pad /	widthMod

			const start	= new Position(l, charIdx + 0)
			const end	= new Position(l, charIdx + 1)
			const range	= new Range(start, end)

			const decoration	= decorations.get(key)
			if (decoration == null)
			{
				decorations.set(key,
				{
					type: window.createTextEditorDecorationType(
					{
						letterSpacing	: `${stretchFactor - 1}ch`,
						rangeBehavior	: DecorationRangeBehavior.ClosedClosed,
						// before	: { contentText: " ".repeat(widthEts) },
					}),
					ranges: [range]
				})
			}
			else
			{
				decoration.ranges.push(range)
			}
		}
	}
	return Array.from(decorations.values())
}

class ElasticEditor
{
	public	_isUnformatted	: boolean = true

	private	_decorations	: Array<IDecoration | undefined>	= []
	private	_timer	: NodeJS.Timeout | undefined
	private	_ignoreUpdate	: boolean = false

	constructor (readonly editor: TextEditor) {}

	public triggerUpdateDocument (conf: Configuration, option: { reset: boolean } = { reset: false }): void
	{
		if (this._timer != null) { clearTimeout(this._timer) }
		if (option.reset) { return }

		const { timeout } = conf
		if (timeout === 0)
		{
			this.updateDocument(conf)
		}
		else
		{
			this._timer = setTimeout(() => { this.updateDocument(conf) }, timeout)
		}
	}

	public updateDocument (conf: Configuration): void
	{
		const site = "updateDocument"
		const { document } = this.editor
		if (document == null || document.isClosed) { return }
		if (this._ignoreUpdate)
		{
			this._ignoreUpdate = false
			return
		}

		if (conf.alignmentSpaces)
		{
			this.editor.edit(builder => {
				this._ignoreUpdate = true

				const textedits = replaceElasticTabstops(document, _conf, true)
				textedits.forEach(te => builder.replace(te.range, te.newText + "\t"))
			}, { undoStopBefore: false, undoStopAfter: false })
			.then(() => this.updateDecorations(conf) , reason => console.log(reason))
		}
		else
		{
			this.updateDecorations(conf)
		}

		this._isUnformatted = false
	}

	private updateDecorations (conf: Configuration): void
	{
		const { spacing, alignmentSpaces: viewSpaces } = conf
		const tabSize	= this.editor.options.tabSize as number
		const textPerLine	= getTextPerLine(this.editor.document)
		const tabstops	= calcElasticTabstopsMonospaced(textPerLine, { ...conf, tabSize, ignoreSpaces: false })

		this.disposeDecorations()
		this._decorations	= formatTabstopPositions(tabstops, viewSpaces ? spacing.pad : -1)
		for (const decoration of this._decorations)
		{
			if (decoration == null) { continue }
			this.editor.setDecorations(decoration.type, decoration.ranges)
		}
	}

	private disposeDecorations (): void
	{
		this._decorations.forEach(d => d?.type.dispose())
		this._decorations.length	= 0
	}

	public dispose(): void
	{
		this.disposeDecorations()
		if (this._timer != null) { clearTimeout(this._timer) }
	}
}
